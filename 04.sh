#!/bin/bash
DATE=`date +%F`
server=$(hostname)
echo -e "Today date is \e[35m $DATE \e[0m"
echo -e "Server hostname is \e[1;34m $server \e[0m"